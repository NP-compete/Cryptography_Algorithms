# Cryptography_Algorithms
Python implementation of cryptographic algorithms

The implemented algorithms are:
- [x] DES
- [x] ElGamal
- [x] Index Calculus
- [x] Poling Hellman
- [x] RSA
- [x] Shanks
